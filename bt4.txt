Các bước làm đồng hồ đếm ngược: Sử dụng hàm setTimeout và clearTimeout

BƯỚC 1: LẤY GIÁ TRỊ BAN ĐẦU TỪ CÁC Ô TEXT INPUT GIỜ, PHÚT, GIÂY
BƯỚC 2: CHUYỂN ĐỔI DỮ LIỆU
Nếu số giây = -1 tức là đã chạy ngược hết số giây, lúc này:
  - giảm số phút xuống 1 đơn vị
 - thiết lập số giây lại 59
 
Nếu số phút = -1 tức là đã chạy ngược hết số phút, lúc này:
- giảm số giờ xuống 1 đơn vị
 - thiết lập số phút lại 59
 
Nếu số giờ = -1 tức là đã hết giờ, lúc này:
- Dừng chương trình thông báo bằng alert là hết giờ